Vue.component('product', {
  props: {
    premium: {
      type: Boolean,
      required: true
    }
  },
  template: `
  <div class="product">
  <div class="product-image">
    <a :href="link">
      <img :src="image">
    </a>
  </div>
  <div class="product-info">

    <h1>{{title}}</h1>
    
    <p v-if="inStock">In Stock</p>
    <!-- <p v-else-if="inventory<=10 && inventory > 0">Almost sold out</p> -->
    <p v-else :class="{lineThrough: !inStock}">Out of Stock</p>
    <!-- <p>{{description}}</p> -->

    <div v-for="(variant, index) in variants" 
        :key="variant.variantId"
        class="color-box"
        :style="{ backgroundColor: variant.variantColor }"
        @mouseover="updateProduct(index)">
    </div>

    <button v-on:click="addToCart" 
            :disabled="!inStock"
            :class="{ disabledButton: !inStock }">Add to cart</button>
    <button @click="removeFromCart">Remove from cart</button>

    <!-- <ul>
      <li v-for="size in sizes">
        {{size}}
      </li>
    </ul> -->

    <span v-if="onSale">{{onSale}}</span>

  </div>

  <product-tabs :reviews="reviews" :details="details" :shipping="shipping"></product-tabs>


  </div>
  `,
  data() {
    return {
      brand: 'Vue Mastery',
      product: 'Socks',
      description: 'Cosy and soft',
      selectedVariant: 0,
      link: 'https://www.vuemastery.com/courses/intro-to-vue-js/attribute-binding',
      inventory: 10,
      details: ['80% cotton', '20% polyester', 'Gender-neutral'],
      variants: [
        {
          variantId: 2234,
          variantColor: "green",
          variantImage: './assets/vmSocks-green.jpg',
          variantQuantity: 10,
          variantOnSale: true,
        },
        {
          variantId: 2235,
          variantColor: 'blue',
          variantImage: './assets/vmSocks-blue.jpg',
          variantQuantity: 0,
          variantOnSale: false,
        }
      ],
      sizes: ['S', 'M', 'L', 'XL', 'XXL'],
      reviews: [],
    }
  },
  methods: {
    addToCart() {
      this.$emit('add-to-cart', this.variants[this.selectedVariant].variantId)
    },
    removeFromCart() {
      console.log('hej')
      this.$emit('remove-from-cart')
    },
    updateProduct(index) {
      this.selectedVariant = index;
      console.log(index)
    },
    
  },
  computed: {
    title() {
      return `${this.brand} ${this.product}`
    },
    image() {
      return this.variants[this.selectedVariant].variantImage
    },
    inStock() {
      return this.variants[this.selectedVariant].variantQuantity
    },
    onSale() {
      return this.variants[this.selectedVariant].variantOnSale ? `${this.title} is on Sale!` : ''
    },
    shipping() {
      if(this.premium) {
        return 'Shipping is free'
      }
      return 2.99
    }
  },
  mounted() {
    eventBus.$on('review-submitted', (productReview) => {
      this.reviews.push(productReview)
    })
  }
})

Vue.component('product-details', {
  props: {
    details: {
      type: Array,
      required: true
    }
  },
  template: 
  `<ul>
    <li v-for="detail in details">{{detail}}</li>
  </ul>`
  
})



Vue.component('product-review', {
  template: `
    <form class="review-form" @submit.prevent="onSubmit">

      <p v-if="errors.length">
        <b>Please correct the following error(s):</b>
        <ul>
          <li v-for="error in errors">{{ error  }}</li>
        </ul>
      </p>
    
      <p>
        <label for="name">Name:</label>
        <input id="name" v-model="name" placeholder="name">
      </p>
      
      <p>
        <label for="review">Review:</label>      
        <textarea id="review" v-model="review" ></textarea>
      </p>
      
      <p>
        <label for="rating">Rating:</label>
        <select id="rating" v-model.number="rating">
          <option>5</option>
          <option>4</option>
          <option>3</option>
          <option>2</option>
          <option>1</option>
        </select>
      </p>

      <div>
        <p>Would You recommend this product?</p>
        <div>
          <input type="radio" id="recommended" v-model="recommendation" name="recommendation" value="recommended">
          <label for="recommended">Yes</label>
        </div>
        <div>
          <input type="radio" id="notRecommended" v-model="recommendation" name="recommendation" value="not-recommended">
          <label for="notRecommended">No</label>
        </div>
      </div>
          
      <p>
        <input type="submit" value="Submit">  
      </p>    
    
    </form>
  `,
  data() {
    return {
      name: null,
      review: null,
      rating: null,
      recommendation: null,
      errors: []
    }
  },
  methods: {
    onSubmit() {
      this.errors = []
      if(this.name && this.review && this.rating && this.recommendation) {
        let productReview = {
          name: this.name,
          review: this.review,
          rating: this.rating,
          recommendation: this.recommendation
        }
        eventBus.$emit('review-submitted', productReview)
        this.name = null,
        this.review = null,
        this.rating = null,
        this.recommendation = null
      } else {
        if(!this.name) this.errors.push("Name required")
        if(!this.review) this.errors.push("Review required")
        if(!this.rating) this.errors.push("Rating required")
        if(!this.recommendation) this.errors.push("recommendation required")
      }
      
    }
  }
})



Vue.component('product-tabs', {
  props: {
    reviews: {
      type: Array,
      required: true
    },
    shipping: {
      type: [Number, String],
      required: true
    },
    details: {
      type: Array,
      required: true
    },
  },
  template: `
    <div> 
      <span class="tab"
            :class="{ activeTab: selectedTab === tab }"
            v-for="(tab, index) in tabs" 
            :key="index"
            @click="selectedTab = tab">
          {{ tab }}      
      </span>

      <div v-show="selectedTab === 'Reviews'">
        <h2>Reviews</h2>
        <p v-if="!reviews.length">There are no reviews yet.</p>
        <ul>
          <li v-for="review in reviews">
            <p>{{  review.name  }}</p>
            <p>Rating: {{  review.rating  }}</p>
            <p>{{  review.review  }}</p>
            <p>{{  review.recommendation  }}</p>
          </li>
        </ul>
      </div>

      <p v-show="selectedTab === 'Shipping'">Shipping: {{ shipping }}</p>

      <product-details v-show="selectedTab === 'Details'" :details="details"></product-details>

      <product-review v-show="selectedTab === 'Make a review'"></product-review>
    </div>
  `,
  data() {
    return {
      tabs: ['Reviews', 'Make a review', 'Shipping', 'Details'],
      selectedTab: 'Reviews'
    }
  }
})


let eventBus = new Vue()



const app = new Vue({
  el: '#app',
  data: {
    premium: true,
    cart: []
  },
  methods: {
    updateCart(id) {
      this.cart.push(id)
    },
    removeFromCart() {
      console.log(this.cart)
      this.cart.pop()
    }
  }
})